# OpenML dataset: Indian-card-payment-data-set

https://www.openml.org/d/43830

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
The card payments data is published by the Reserve Bank of India on a monthly basis. The statistics cover the methods of payment used in retail transactions and ATM transactions in India. It constitutes payments via debit cards, credit cards, ATMs etc, . It can can be used to check trend of card based payment in India.
Content
The data contains monthly statistics of the following information from Apr'2011 to Aug'2019

Number of ATM deployed on site by the bank.
Number of ATM deployed off site by the bank.
Number of POS deployed online by the bank
Number of POS deployed offline by the bank
Total number of credit cards issued outstanding (after adjusting the number of cards withdrawan/cancelled).
Total number of financial transactions done by the credit card issued by the bank at ATMs
Total number of financial transactions done by the credit card issued by the bank at POS terminals
Total value of financial transactions done by the credit card issued by the bank at ATMs
Total value of financial transactions done by the credit card issued by the bank at POS terminals.
Total number of debit cards issued outstanding (after adjusting the number of cards withdrawan/cancelled).
Total number of financial transactions done by the debit card issued by the bank at ATMs
Total number of financial transactions done by the debit card issued by the bank at POS terminals
Total value of financial transactions done by the debit card issued by the bank at ATMs
Total value of financial transactions done by the debit card issued by the bank at POS terminals.

Acknowledgements
The data is scraped from RBI monthly statistics
https://www.rbi.org.in/scripts/ATMView.aspx
More details on how this data is collected and cleaned is documented in this kernel
https://www.kaggle.com/karvalo/indian-card-payment-data-gathering-and-analysis

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43830) of an [OpenML dataset](https://www.openml.org/d/43830). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43830/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43830/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43830/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

